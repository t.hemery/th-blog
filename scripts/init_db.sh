vendor/bin/doctrine orm:schema-tool:drop --force
php bin/console doctrine:database:drop --force
rm src/Migrations/*
del src/Migrations/*
echo 'assurez vous que le fichier src/Migrations soit vide'
echo 'appuyez sur Entrer pour valider'
read
php bin/console doctrine:database:create
vendor/bin/doctrine orm:schema-tool:update --force --dump-sql
php bin/console make:migration
php bin/console doctrine:migration:migrate
php bin/console doctrine:fixtures:load
echo 'press enter to exit'
read