<?php

namespace App\Controller\Posts;

use App\Entity\Posts\Post;
use App\Form\Posts\PostType;
use App\Repository\Posts\PostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/posts/post")
 */
class PostController extends AbstractController
{
    /**
     * @Route("/", name="posts_post_index", methods={"GET"})
     */
    public function index(PostRepository $postRepository): Response
    {
        return $this->render('posts/post/index.html.twig', [
            'posts' => $postRepository->findAll(),
            'bpost' => New Post()
        ]);
    }

    /**
     * @Route("/new", name="posts_post_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $post = new Post();

        $this->denyAccessUnlessGranted('POST_CREATE', $post);

        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $post->setAuthor($this->getUser());
            $entityManager->persist($post);
            $entityManager->flush();

            return $this->redirectToRoute('posts_post_index');
        }

        return $this->render('posts/post/new.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="posts_post_show", methods={"GET"})
     */
    public function show(Post $post): Response
    {
        $this->denyAccessUnlessGranted('POST_VIEW', $post);

        return $this->render('posts/post/show.html.twig', [
            'post' => $post,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="posts_post_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Post $post): Response
    {
        $this->denyAccessUnlessGranted('POST_EDIT', $post);

        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('posts_post_index');
        }

        return $this->render('posts/post/edit.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="posts_post_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Post $post): Response
    {
        $this->denyAccessUnlessGranted('POST_DELETE', $post);

        if ($this->isCsrfTokenValid('delete'.$post->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($post);
            $entityManager->flush();
        }

        return $this->redirectToRoute('posts_post_index');
    }
}
