<?php

namespace App\Security\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Security;

class PostVoter extends Voter
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports($attribute, $subject)
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, ['POST_CREATE', 'POST_DELETE','POST_EDIT', 'POST_VIEW'])
            && $subject instanceof \App\Entity\Posts\Post;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case 'POST_CREATE':
                return $this->security->isGranted('ROLE_ADMIN');
                break;
            case 'POST_EDIT':
                return $this->security->isGranted('ROLE_ADMIN') || $user == $subject->getAuthor();
                break;
            case 'POST_DELETE':
                return $this->security->isGranted('ROLE_ADMIN') || $user == $subject->getAuthor();
                break;
            case 'POST_VIEW':
                return true;
                break;
        }

        return false;
    }
}
