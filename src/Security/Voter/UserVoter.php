<?php

namespace App\Security\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Security;

class UserVoter extends Voter
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports($attribute, $subject)
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, ['USER_CREATE','USER_EDIT', 'USER_VIEW', 'USER_DELETE'])
            && $subject instanceof \App\Entity\User;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case 'USER_CREATE':
                return $this->security->isGranted('ROLE_ADMIN');
                break;
            case 'USER_EDIT':
                return $user == $subject;
                break;
            case 'USER_VIEW':
                return $user == $subject;
                break;
            case 'USER_DELETE':
                return $this->security->isGranted('ROLE_ADMIN') || $user == $subject;
                break;
        }

        return false;
    }
}
