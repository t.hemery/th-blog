<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

use App\Entity\User;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setUsername("tugo");
        $user->setPassword('$argon2id$v=19$m=65536,t=4,p=1$RVppendIRG9kdGhxbDd2UQ$X1rArrOyZlzkJf3FctBVeS2/Q0KKs9t5x0xt2wCHAjI');
        $user->addRole("ROLE_ADMIN");
        $manager->persist($user);

        $user = new User();
        $user->setUsername("TheMarvelous");
        $user->setPassword('$argon2id$v=19$m=65536,t=4,p=1$ak9oNk4vay9oMXRINHh3dA$wWeI/3dI11dW4acd1D/Li1vYV1cWk8ZwPJTrMM1oZQY');
        $manager->persist($user);

        $user = new User();
        $user->setUsername("dummy");
        $user->setPassword('$argon2id$v=19$m=65536,t=4,p=1$Vm5OeDFmc2F0SXNycjNBYw$Dek7jHuzhzVyRvV6G8hunAR2yX8UcdxEbBPbE9ACqjY');
        $manager->persist($user);

        $manager->flush();
    }
}
